import 'package:flutter/cupertino.dart';

double getScreenWidth(context) {
  double width = MediaQuery.of(context).size.width;
  return width;
}

double getScreenHeight(context) {
  double height = MediaQuery.of(context).size.height;
  return height;
}

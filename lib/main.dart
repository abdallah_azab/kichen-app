import 'package:app_app_kitchen/provider/history_data_provider.dart';
import 'package:app_app_kitchen/provider/home_data_provider.dart';
import 'package:app_app_kitchen/server_socket/get_ip_network/provider_get_ip.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/screens/intro_page/fiest_page_open_server.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => HistoryProvider()),
        ChangeNotifierProvider(create: (_) => HomeProvider()),
        ChangeNotifierProvider(create: (_) => GetIpNetworkProvider()),
      ],
      child: MaterialApp(
        theme: ThemeData(
            scaffoldBackgroundColor: MyColors.offWhite,
            appBarTheme: AppBarTheme(
              toolbarHeight: 85,
              color: MyColors.darkBlue,
              elevation: 0.0,
            )),
        debugShowCheckedModeBanner: false,
        title: 'app_kitchen App',
        // home: const HomePage2(),
        // home: const HomePage(),
        home: const StartApp(),
      ),
    );
  }
}


import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive/src/hive_impl.dart';

class HistoryProvider with ChangeNotifier {

  final HiveInterface _hive = HiveImpl();
  List historyOrder = [];

  initalData() async {
    var dir = await getApplicationDocumentsDirectory();
    debugPrint('path ==== ' + dir.path);
    _hive.init(dir.path);
    await _hive.openBox('myOrders1');
    var box = _hive.box('myOrders1');
    historyOrder = box.values.toList();
    debugPrint(historyOrder.toString());
    notifyListeners();
  }

  addToHistoryDB(data) async {
    var dir = await getApplicationDocumentsDirectory();
    debugPrint('path ==== ' + dir.path);
    _hive.init(dir.path);
    await _hive.openBox('myOrders1');
    var box = _hive.box('myOrders1');
    debugPrint(
        "==== length Before Add ====" + box.values.toList().length.toString());
    if (box.values.toList().length > 100) {
      box.deleteAt(0);
      debugPrint("length box " + box.length.toString());
      debugPrint("success to remove from list ");
    }
    box.add(data);

    historyOrder = box.values.toList();
    debugPrint('==== success to add to Data Base =====');
    debugPrint("==== length ====" + box.values.toList().length.toString());
    debugPrint(historyOrder.length.toString());
    notifyListeners();
  }

  removeDataBase() async {
    await _hive.openBox('myOrders1');

    print(_hive.box('myOrders1').keys);
    _hive.box('myOrders1').clear();
    historyOrder.clear();
    notifyListeners();
  }

  removeOneOrder(index) {
    historyOrder.removeAt(index);
    _hive.box('myOrders1').deleteAt(index);
    notifyListeners();
  }
}

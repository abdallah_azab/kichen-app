import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive/src/hive_impl.dart';

class HomeProvider with ChangeNotifier {
  final HiveInterface _hive = HiveImpl();
  List homeOrders = [];

  initialDataHome() async {
    var dir = await getApplicationDocumentsDirectory();
    _hive.init(dir.path);
    await _hive.openBox('myOrdersHome');
    var box = _hive.box('myOrdersHome');
    homeOrders = box.values.toList();
    notifyListeners();
  }

  addToHomeDB(data) async {
    var dir = await getApplicationDocumentsDirectory();
    _hive.init(dir.path);
    await _hive.openBox('myOrdersHome');
    var box = _hive.box('myOrdersHome');

    box.add(data.toJson());

    homeOrders = box.values.toList();

    debugPrint("Data Base Home length==== " + homeOrders.length.toString());
    notifyListeners();
  }

  removeDataBaseHome() async {
    await _hive.openBox('myOrdersHome');
    _hive.box('myOrdersHome').clear();
    homeOrders.clear();
    notifyListeners();
  }

  removeOneOrderFromHome(index) {
    homeOrders.removeAt(index);

    _hive.box('myOrdersHome').deleteAt(index);
    notifyListeners();
  }

  addToHomeDBInIndex(index, data) async {
    var dir = await getApplicationDocumentsDirectory();
    _hive.init(dir.path);
    await _hive.openBox('myOrdersHome');
    var box = _hive.box('myOrdersHome');
    box.getAt(index);
    box.putAt(index, data.toJson());
    box.getAt(index);
    homeOrders = box.values.toList();
    debugPrint(
        "Data Base Home length====inswx==== " + homeOrders.length.toString());
    notifyListeners();
  }
}

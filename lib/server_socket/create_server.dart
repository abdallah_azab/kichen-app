import 'dart:async';
import 'dart:io';

import 'dart:typed_data';

import 'package:flutter/cupertino.dart';

import 'package:provider/provider.dart';

import 'get_ip_network/provider_get_ip.dart';
import 'model.dart';

class Server {
  Server(
      {this.onError,
      this.onData,
      required this.context,
      required this.ipSocket});

  BuildContext context;

  String ipSocket;

  Uint8ListCallback? onData;
  DynamicCallback? onError;
  ServerSocket? server;
  bool running = false;
  List<Socket> sockets = [];

  // String ?ipSocket ;
  start() async {
    ipSocket =
        Provider.of<GetIpNetworkProvider>(context, listen: false).ipSocket;
    runZoned(() async {
      print("socket ip =======  " + ipSocket);
      server = await ServerSocket.bind(ipSocket, 4040);
      running = true;
      server!.listen(onRequest);
    }, onError: (e) {
      onError!(e);
    });
  }

  stop() async {
    await server!.close();
    server = null;
    running = false;
  }

  broadCast(String message) {
    onData!(Uint8List.fromList('Broadcasting : $message'.codeUnits));
    for (Socket socket in sockets) {
      socket.write(message + '\n');
    }
  }

  onRequest(Socket socket) {
    if (!sockets.contains(socket)) {
      sockets.add(socket);
      print("address clint is  " + socket.remoteAddress.address);
    }
    socket.listen((Uint8List data) {
      print(String.fromCharCodes(data));
      onData!(data);
    });
  }
}

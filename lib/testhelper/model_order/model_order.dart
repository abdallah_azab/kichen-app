class ModelOrder {
  ModelOrder({
    required this.orders,
  });

  late final List<Orders> orders;

  ModelOrder.fromJson(Map<String, dynamic> json) {
    orders = List.from(json['orders']).map((e) => Orders.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['orders'] = orders.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Orders {
  Orders({
    required this.customer,
    required this.tableNumber,
    required this.posOpening,
    required this.casher,
    required this.orderStatus,
    required this.status,
    required this.time,
    required this.orderNumber,
    required this.items,
  });

  late final String customer;
  late final String tableNumber;
  late final String posOpening;
  late final String casher;
  late final String orderStatus;
  late final String status;
  late final String time;
  late final String orderNumber;
  late final List<Items> items;

  Orders.fromJson(Map<String, dynamic> json) {
    customer = json['customer'];
    tableNumber = json['table_number'];
    posOpening = json['pos_opening'];
    casher = json['casher'];
    orderStatus = json['order_status'];
    status = json['status'];
    time = json['time'];
    orderNumber = json['order_number'];
    items = List.from(json['items']).map((e) => Items.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['customer'] = customer;
    _data['table_number'] = tableNumber;
    _data['pos_opening'] = posOpening;
    _data['casher'] = casher;
    _data['order_status'] = orderStatus;
    _data['status'] = status;
    _data['time'] = time;
    _data['order_number'] = orderNumber;
    _data['items'] = items.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Items {
  Items(
      {required this.itemCode,
      required this.itemName,
      required this.isSup,
      required this.isCustom,
      required this.description,
      required this.qty,
      required this.stockUom,
      this.status_item});

  late final String itemCode;
  late final String itemName;
  late final int isSup;
  late final int isCustom;
  late final String description;
  late final int qty;
  late final String stockUom;
  var status_item;

  Items.fromJson(Map<String, dynamic> json) {
    itemCode = json['item_code'];
    itemName = json['item_name'];
    isSup = json['is_sup'];
    isCustom = json['is_custom'];
    description = json['description'];
    qty = json['qty'];
    stockUom = json['stock_uom'];
    status_item = json['status_item'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['item_code'] = itemCode;
    _data['item_name'] = itemName;
    _data['is_sup'] = isSup;
    _data['is_custom'] = isCustom;
    _data['description'] = description;
    _data['qty'] = qty;
    _data['stock_uom'] = stockUom;
    _data['status_item'] = status_item;
    return _data;
  }
}

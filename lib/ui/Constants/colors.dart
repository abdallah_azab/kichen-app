import 'package:flutter/material.dart';

class MyColors {
  static Color black = const Color(0xff000000);
  static Color blue = const Color(0xff5270AF);
  static Color gray = const Color(0xff8C8C8C);
  static Color lightGreen = const Color(0xff8CE8C0);
  static Color darkGray = const Color(0xff707070);
  static Color offWhite = const Color(0xffEEEEEE);
  static Color orange = const Color(0xffF6A320);
  static Color darkOrange = const Color(0xffFF9C00);
  static Color green = const Color(0xff6CBF9B);
  static Color darkBlue = const Color(0xff213152);
  static Color white = const Color(0xffFFFFFF);
}

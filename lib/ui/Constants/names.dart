class ConstNames {
  static const String Home_Page = 'Home';
  static const String History_Page = 'History';
  static const String New_Sound = 'new.mp3';
  static const String Changed_Sound = 'change.wav';
  static const String Done_Sound = 'done.wav';
  static const String Pop_Sound = 'pop.mp3';
}

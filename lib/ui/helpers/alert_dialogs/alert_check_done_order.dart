import 'package:app_app_kitchen/helper_fun/get_app_size.dart';
import 'package:flutter/material.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/info_order.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';

showAlertDoneOrder(BuildContext context, onTap,
    {tableNumber, nameOwnOrder, orderNumber, timeOrder, typeOrder}) {
  double heightAlert = getScreenHeight(context) / 4.5;
  double widthAlert = getScreenWidth(context) / 4.5;
  AlertDialog alertDialog = AlertDialog(
    scrollable: true,
    titlePadding: EdgeInsets.zero,
    contentPadding: const EdgeInsets.all(8),
    title: Container(
      padding: const EdgeInsets.all(15),
      color: MyColors.green,
      child: Center(
        child: MyText(title: 'Confirmation', size: 20),
      ),
    ),
    elevation: 2,
    content: SizedBox(
      height: heightAlert,
      width: widthAlert,
      child: Column(
        children: [
          SizedBox(
              // height: heightAlert / 3.5,
              // color: Colors.green,
              child: infoOrder(
                  tableNumber: tableNumber.toString(),
                  nameOwnOrder: nameOwnOrder.toString(),
                  orderNumber: orderNumber.toString(),
                  timeExpected: '',
                  timeOrder: timeOrder.toString(),
                  typeOrder: typeOrder.toString())),
          Divider(
            color: MyColors.darkGray,
          ),
          InkWell(
            onTap: onTap,
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: MyColors.green,
                ),
                child: MyText(title: 'OK', colorText: MyColors.white, size: 18),
              ),
            ),
          )
        ],
      ),
    ),
    backgroundColor: MyColors.offWhite,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
  );
  showDialog(
    context: context,
    builder: (context) => alertDialog,
  );
}

import 'package:flutter/material.dart';
import 'package:app_app_kitchen/helper_fun/get_app_size.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';

showAlertDeleteHistory(BuildContext context, removeHistory) {
  double heightAlert = getScreenHeight(context) / 8;
  double widthAlert = getScreenWidth(context) / 7;
  AlertDialog alertDialog = AlertDialog(
    scrollable: true,
    titlePadding: EdgeInsets.zero,
    contentPadding: const EdgeInsets.all(8),
    title: Container(
      padding: const EdgeInsets.all(15),
      color: MyColors.green,
      child: Center(
        child: MyText(title: 'Delete History', size: 20),
      ),
    ),
    elevation: 2,
    content: InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: SizedBox(
        height: heightAlert,
        width: widthAlert,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            MyText(title: "Are you Sure ", size: 17, colorText: MyColors.black),
          ],
        ),
      ),
    ),
    actions: <Widget>[
      TextButton(
        onPressed: () => Navigator.of(context).pop(false),
        child: MyText(title: 'No', size: 17, colorText: Colors.red),
      ),
      TextButton(
        // onPressed: removeHistory ,
        onPressed: () {
          removeHistory();
          Navigator.pop(context);
        },

        child: MyText(title: 'yes', size: 17, colorText: Colors.blue),
      ),
    ],
    backgroundColor: MyColors.offWhite,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
  );
  showDialog(
    context: context,
    builder: (context) => alertDialog,
  );
}

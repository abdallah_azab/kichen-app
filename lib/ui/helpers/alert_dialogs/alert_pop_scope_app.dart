import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PopScopeApp extends StatelessWidget {
  final child;

  const PopScopeApp({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onWillPop(context),
      child: child,
    );
  }

  Future<bool> _onWillPop(context) async {
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text('Are you sureto exit an App ?'),
            content: const Text('The history data will deleted'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: MyText(title: 'No', size: 17, colorText: Colors.red),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: MyText(title: 'yes', size: 17, colorText: Colors.blue),
              ),
            ],
          ),
        )) ??
        false;
  }
}

import 'package:flutter/material.dart';
import 'package:app_app_kitchen/helper_fun/get_app_size.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';
import 'package:flutter/services.dart';
import 'package:new_keyboard_shortcuts/keyboard_shortcuts.dart';

showAlertSettings(BuildContext context, String? ipAddress) {
  double heightAlert = getScreenHeight(context) / 9.5;
  double widthAlert = getScreenWidth(context) / 7.5;
  AlertDialog alertDialog = AlertDialog(
    scrollable: true,
    titlePadding: EdgeInsets.zero,
    contentPadding: const EdgeInsets.all(8),
    title: Container(
      padding: const EdgeInsets.all(15),
      color: MyColors.green,
      child: Center(
        child: MyText(title: 'Settings', size: 20),
      ),
    ),
    elevation: 2,
    content: InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: SizedBox(
        height: heightAlert,
        width: widthAlert,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            MyText(title: "Ip address", size: 17, colorText: MyColors.black),
            KeyBoardShortcuts(
              keysToPress: {LogicalKeyboardKey.digit1},
              onKeysPressed: (){
                Navigator.pop(context);
              },
              child: InkWell(

                child: Card(
                  elevation: 2.0,
                  color: MyColors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: MyText(title: ipAddress!, colorText: MyColors.gray),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    ),
    backgroundColor: MyColors.offWhite,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
  );
  showDialog(
    context: context,
    builder: (context) => alertDialog,
  );
}

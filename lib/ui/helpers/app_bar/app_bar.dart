import 'package:app_app_kitchen/ui/helpers/alert_dialogs/alert_delete_history.dart';
import 'package:app_app_kitchen/ui/helpers/alert_dialogs/alert_settings.dart';
import 'package:app_app_kitchen/ui/screens/history_screen/history_page.dart';
import 'package:flutter/material.dart';
import 'package:app_app_kitchen/helper_fun/get_app_size.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/Constants/names.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';
import 'package:flutter/services.dart';
import 'package:new_keyboard_shortcuts/keyboard_shortcuts.dart';

myAppBar(
    {required context,
    required String title,
    onTapBack,
    onTapNext,
    onRemoveHistory,
    String? ipAddress}) {
  return AppBar(
    actions: [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
        child: KeyBoardShortcuts(
            // keysToPress: {LogicalKeyboardKey.shiftLeft,LogicalKeyboardKey.keyD},
            keysToPress: {LogicalKeyboardKey.digit6},
            onKeysPressed: onTapNext,
            child: InkWell(
              onTap: onTapNext,
              child: SizedBox(
                  height: 70, child: Image.asset('assets/icons/next.png')),
            )),
      ),
    ],
    title: title == ConstNames.History_Page
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: InkWell(
              // onTap: onRemoveHistory,
              onTap: () {
                showAlertDeleteHistory(context, onRemoveHistory);
              },
              child: Container(
                // height: 90,
                width: 70,
                padding: const EdgeInsets.only(
                    right: 10, left: 10, bottom: 0, top: 0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: MyColors.darkBlue),
                child: const Center(
                    child: Icon(
                  Icons.delete,
                  color: Colors.red,
                  size: 50,
                )),
              ),
            ),
          )
        : const SizedBox(),
    leading: Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          KeyBoardShortcuts(
              keysToPress: {LogicalKeyboardKey.digit4},
              onKeysPressed: onTapBack,
              child: InkWell(
                onTap: onTapBack,
                child: SizedBox(
                    height: 70, child: Image.asset('assets/icons/back.png')),
              )),
          KeyBoardShortcuts(
            keysToPress: {LogicalKeyboardKey.digit5},
            onKeysPressed: () {
              if (title == ConstNames.History_Page) {
                Navigator.pop(context);
              } else if (title == ConstNames.Home_Page) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const HistoryPage(),
                    ));
              }
            },
            child: InkWell(
              onTap: () {
                if (title == ConstNames.History_Page) {
                  Navigator.pop(context);
                } else if (title == ConstNames.Home_Page) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const HistoryPage(),
                      ));
                }
              },
              child: title == ConstNames.Home_Page
                  ? Container(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(13),
                          color: MyColors.lightGreen),
                      child: Center(
                          child: MyText(
                              title: ConstNames.History_Page,
                              colorText: MyColors.white)),
                    )
                  : Container(
                      padding: const EdgeInsets.only(
                          right: 10, left: 10, bottom: 10, top: 0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: MyColors.darkOrange),
                      child: Center(
                          child: Icon(
                        Icons.home,
                        color: MyColors.white,
                        size: 55,
                      )),
                    ),
            ),
          ),
          KeyBoardShortcuts(
            keysToPress: {LogicalKeyboardKey.digit0},
            onKeysPressed: () {
              showAlertSettings(context, ipAddress);
            },
            child: InkWell(
                onTap: () {
                  showAlertSettings(context, ipAddress);
                },
                child: SizedBox(
                    height: 70,
                    child: Image.asset('assets/icons/setting.png'))),
          )
        ],
      ),
    ),
    leadingWidth: getScreenWidth(context) / 2,
  );
}

import 'package:app_app_kitchen/ui/helpers/alert_dialogs/alert_check_done_order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_app_kitchen/helper_fun/get_app_size.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';
import '../text.dart';

Widget DoneButton(
    {required context,
    color,
    required onTap,
    tableNumber,
    nameOwnOrder,
    orderNumber,
    timeOrder,
    typeOrder}) {
  double heightScreen = getScreenHeight(context);
  return InkWell(
    onTap: () {
      showAlertDoneOrder(context, onTap,
          typeOrder: typeOrder,
          timeOrder: timeOrder,
          nameOwnOrder: nameOwnOrder,
          orderNumber: orderNumber,
          tableNumber: tableNumber);
    },
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0.5),
      child: Container(
        height: heightScreen / 14,
        decoration: BoxDecoration(
            color: color ?? MyColors.lightGreen,
            borderRadius: BorderRadius.circular(5)),
        child: Center(
            child: MyText(title: 'Done', colorText: MyColors.white, size: 16)),
      ),
    ),
  );
}

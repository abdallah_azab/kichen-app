import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_app_kitchen/helper_fun/get_app_size.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';

Widget continu({required context, colorStateOrder}) {
  double height = getScreenHeight(context) / 12;
  double width = getScreenWidth(context) / 4.6;
  return Padding(
    padding: const EdgeInsets.all(0),
    child: Row(
      children: [
        // ====== the color explain state of order ========
        Card(
          elevation: .5,
          child: Container(
            padding: const EdgeInsets.all(4),
            height: height,
            width: 8,
            decoration: BoxDecoration(
                color: colorStateOrder ?? MyColors.lightGreen,
                borderRadius: BorderRadius.circular(30)),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Expanded(
            child: Container(
          height: height,
          // color: Colors.deepPurpleAccent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(title: '4' + ' x ' + 'Beef Pizza', size: 16),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                child: MyText(
                    title: 'Diameter 50 /Extra / Double  ',
                    colorText: MyColors.gray),
              )
            ],
          ),
        ))
      ],
    ),
  );
}

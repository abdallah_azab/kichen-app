import 'package:flutter/cupertino.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';
import 'package:flutter/material.dart';

Widget infoOrder({
  timeExpected,
  timeOrder,
  nameOwnOrder,
  typeOrder,
  orderNumber,
  tableNumber,
}) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      rowTextInfo(titleOne: '$timeOrder  Pm', titleTow: '$timeExpected '),
      const SizedBox(
        height: 4,
      ),
      rowTextInfo(
          titleOne: nameOwnOrder.toString(), titleTow: 'Order #$orderNumber'),
      Padding(
        padding: const EdgeInsets.only(top: 15, bottom: 8),
        child: rowTextInfo(
            titleTow: 'table #$tableNumber',
            titleOne: typeOrder.toString(),
            colorText: MyColors.black,
            size: 16),
      ),
    ],
  );
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_app_kitchen/helper_fun/get_app_size.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';

Widget order(
    {required context,
    Color? colorStateOrder,
    // var colorStateOrder,
    required String nameItem,
      bool ?overLine ,
    required String qun,
    required String des,
    int? isCustom}) {
  double height = getScreenHeight(context) / 11.2;
  return Row(
    children: [
      // ====== the color explain state of order ========
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4),
        child: Container(
          height: height,
          width: 8,
          decoration: BoxDecoration(
              color: colorStateOrder ?? MyColors.lightGreen,
              borderRadius: BorderRadius.circular(20)),
        ),
      ),
      const SizedBox(
        width: 10,
      ),
      Expanded(
          child: SizedBox(
        height: height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MyText(title: '${qun} x ${nameItem}', size: 16,overLine: overLine!),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
              child: MyText(
                  title: des,
                  colorText: MyColors.gray),
            ),
            isCustom != 0
                ? Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                    child: MyText(
                        title: 'Is Custom',
                        colorText: MyColors.gray),
                  )
                : const SizedBox()
          ],
        ),
      ))
    ],
  );
}

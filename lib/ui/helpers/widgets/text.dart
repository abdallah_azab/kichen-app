import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:app_app_kitchen/ui/Constants/colors.dart';

Widget MyText({required String title, Color? colorText, double? size,bool overLine =false }) {
  return Text(
    title.toString(),
    style: TextStyle(
        fontSize: size ?? 14,
        fontWeight: FontWeight.w700,
        decoration: overLine== true ?TextDecoration.overline :TextDecoration.none,
        color: colorText ?? MyColors.black),
  );
}

Widget rowTextInfo({titleOne, titleTow, colorText, double? size}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      MyText(
          title: titleOne ?? '',
          colorText: colorText ?? MyColors.black,
          size: size),
      MyText(
          title: titleTow ?? '',
          colorText: colorText ?? MyColors.black,
          size: size),
    ],
  );
}

Widget orderInfoInAlert({
  required context,
  colorStateOrder,
}) {
  // double height = getScreenHeight(context) / 10;
  // double width = getScreenWidth(context) / 4.6;
  return Padding(
    padding: const EdgeInsets.all(4.0),
    child: Row(
      children: [
        Expanded(
            child: Container(
          child: MyText(title: '4' + ' x ' + 'Beef Pizza'),
        ))
      ],
    ),
  );
}

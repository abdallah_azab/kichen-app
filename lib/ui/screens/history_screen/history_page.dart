import 'package:app_app_kitchen/helper_fun/get_app_size.dart';
import 'package:app_app_kitchen/provider/history_data_provider.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/Constants/names.dart';
import 'package:app_app_kitchen/ui/helpers/app_bar/app_bar.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/info_order.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/order.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HistoryPage extends StatefulWidget {
  const HistoryPage({Key? key}) : super(key: key);

  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  final ScrollController _scrollController = ScrollController();
  late double position;
  List? modelOrder;

  @override
  void initState() {
    super.initState();
    Provider.of<HistoryProvider>(context, listen: false).initalData();
    modelOrder =
        Provider.of<HistoryProvider>(context, listen: false).historyOrder;
    position = _scrollController.initialScrollOffset;
    _scrollController.addListener(() {
      position = _scrollController.offset;
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double widthScreen = getScreenWidth(context);
    double heightScreen = getScreenHeight(context);
    return Scaffold(
      appBar: myAppBar(
          context: context,
          title: ConstNames.History_Page,
          onTapNext: () {
            _scrollController.animateTo(position + 170,
                duration: const Duration(milliseconds: 250),
                curve: Curves.bounceInOut);
          },
          onTapBack: () {
            _scrollController.animateTo(position - 170,
                duration: const Duration(milliseconds: 250),
                curve: Curves.bounceInOut);
          },
          onRemoveHistory: () {
            Provider.of<HistoryProvider>(context, listen: false)
                .removeDataBase();
            setState(() {
              modelOrder!.clear();
            });
          }),
      body: modelOrder == null || modelOrder!.isEmpty
          // 1==1
          ? Center(
              child: MyText(title: "No History until Now", size: 20),
            )
          : Center(
              child: ListView.separated(
                controller: _scrollController,
                scrollDirection: Axis.horizontal,
                itemCount: modelOrder!.length,
                itemBuilder: (context, indexOfAllOrder) {
                  return Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: heightScreen,
                          width: widthScreen / 5,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: MyColors.white),
                          child: Column(
                            children: [
                              // ===== this header ======  Done ==
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 0.5),
                                child: Container(
                                  height: heightScreen / 14,
                                  decoration: BoxDecoration(
                                      color: modelOrder![indexOfAllOrder]
                                                  ['status'] !=
                                              'New'
                                          ? MyColors.orange
                                          : MyColors.lightGreen,
                                      borderRadius: const BorderRadius.only(
                                          bottomLeft: Radius.circular(1),
                                          bottomRight: Radius.circular(1))),
                                  child: Center(
                                      child: MyText(
                                          title: modelOrder![indexOfAllOrder]
                                              ['status'])),
                                ),
                              ),
                              // === this info of order ===== Done ==

                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 8, right: 8, top: 10, bottom: 1),
                                child: infoOrder(
                                    nameOwnOrder: modelOrder![indexOfAllOrder]
                                        ['casher'],
                                    // numberOrder: orders[indexOfAllOrder].toString(),
                                    orderNumber: modelOrder![indexOfAllOrder]
                                        ['order_number'],
                                    tableNumber: modelOrder![indexOfAllOrder]
                                            ['table_number']
                                        .toString(),
                                    timeExpected: '',
                                    timeOrder: modelOrder![indexOfAllOrder]
                                        ['time'],
                                    typeOrder: modelOrder![indexOfAllOrder]
                                        ['order_status']),
                              ),
                              Divider(
                                color: MyColors.gray,
                              ),
                              // ====== this list of orders =====
                              Expanded(
                                child: ListView.separated(
                                  // it mean if items more than 5 don't show else 5
                                  itemCount: modelOrder![indexOfAllOrder]
                                          ['items']
                                      .length,

                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 2),
                                  shrinkWrap: true,
                                  itemBuilder: (context, indexOfItems) {
                                    return order(
                                        isCustom: modelOrder![indexOfAllOrder]
                                                ['items'][indexOfItems]
                                            ['is_custom'],
                                        context: context,
                                        colorStateOrder: MyColors.lightGreen,
                                        des: modelOrder![indexOfAllOrder]
                                                ['items'][indexOfItems]
                                            ['description'],
                                        nameItem: modelOrder![indexOfAllOrder]
                                                ['items'][indexOfItems]
                                            ['item_name'],
                                        overLine: false,
                                        qun: modelOrder![indexOfAllOrder]
                                                ['items'][indexOfItems]['qty']
                                            .toString());
                                  },
                                  //=====  Divider ====
                                  separatorBuilder:
                                      (context, indexOfItemsSeparator) {
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 0, horizontal: 4),
                                      child: Divider(
                                        color: MyColors.gray,
                                      ),
                                    );
                                  },
                                ),
                              ),

                              const SizedBox()
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                },
                shrinkWrap: true,
                separatorBuilder: (context, indexOfAllOrderSeparator) {
                  return const SizedBox(
                    width: 15,
                  );
                },
              ),
            ),
    );
  }
}

// import 'dart:convert';
//
// import 'package:app_app_kitchen/helper_fun/get_app_size.dart';
// import 'package:app_app_kitchen/provider/history_data_provider.dart';
// import 'package:app_app_kitchen/testhelper/model_order/model_order.dart';
//
// import 'package:app_app_kitchen/ui/Constants/colors.dart';
// import 'package:app_app_kitchen/ui/Constants/names.dart';
// import 'package:app_app_kitchen/ui/helpers/alert_dialogs/alert_pop_scope_app.dart';
// import 'package:app_app_kitchen/ui/helpers/app_bar/app_bar.dart';
// import 'package:app_app_kitchen/ui/helpers/widgets/buttons/done_button.dart';
// import 'package:app_app_kitchen/ui/helpers/widgets/info_order.dart';
// import 'package:app_app_kitchen/ui/helpers/widgets/order.dart';
// import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:provider/provider.dart';
//
// class HomePage2 extends StatefulWidget {
//   const HomePage2({Key? key}) : super(key: key);
//
//   @override
//   _HomePage2State createState() => _HomePage2State();
// }
//
// class _HomePage2State extends State<HomePage2> {
//   final ScrollController _scrollController = ScrollController();
//   late double position;
//
//   // List<OrderModel> orderInHome = [];
//
//   List<Orders>? modelOrder;
//   int? lengthTaps;
//
//   Future readeData() async {
//     final String response =
//         await rootBundle.loadString('assets/test_data_json/order.json');
//     // print("Response ===== " + response);
//     final data = json.decode(response);
//
//     setState(() {
//       modelOrder = ModelOrder.fromJson(data).orders;
//     });
//
//     print(modelOrder![1].casher);
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     readeData();
//     //orderInHome = orders;
//     position = _scrollController.initialScrollOffset;
//     _scrollController.addListener(() {
//       // print("offset = ${_scrollController.offset}");
//
//       position = _scrollController.offset;
//     });
//   }
//
//   @override
//   void dispose() {
//     _scrollController.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double widthScreen = getScreenWidth(context);
//     double heightScreen = getScreenHeight(context);
//     // print("order" + orders.length.toString());
//     //print("in home " + orderInHome.length.toString());
//     return PopScopeApp(
//       child: Scaffold(
//           appBar: myAppBar(
//             context: context,
//             title: ScreenName.Home_Page,
//             onTapNext: () {
//               _scrollController.animateTo(position + 170,
//                   duration: const Duration(milliseconds: 250),
//                   curve: Curves.bounceInOut);
//             },
//             onTapBack: () {
//               _scrollController.animateTo(position - 170,
//                   duration: const Duration(milliseconds: 250),
//                   curve: Curves.bounceInOut);
//             },
//           ),
//           body:
//               // orderInHome.isEmpty ?Center(child: MyText(title: "No Orders until Now",size: 20),)
//               modelOrder != null
//                   ? Center(
//                       child: ListView.separated(
//                         controller: _scrollController,
//                         scrollDirection: Axis.horizontal,
//                         // itemCount: modelOrder.length,
//                         itemCount: modelOrder!.length,
//                         itemBuilder: (context, indexOfAllOrder) {
//                           if (modelOrder![indexOfAllOrder].items.length > 5) {
//                             int lengthTaps =
//                                 modelOrder![indexOfAllOrder].items.length ~/ 5;
//                             debugPrint(lengthTaps.toString());
//                           }
//                           return Row(
//                             children: [
//                               Padding(
//                                 padding: const EdgeInsets.all(8.0),
//                                 child: Container(
//                                   height: heightScreen,
//                                   width: widthScreen / 5,
//                                   decoration: BoxDecoration(
//                                       borderRadius: BorderRadius.circular(10),
//                                       color: MyColors.white),
//                                   child: Column(
//                                     children: [
//                                       // ===== this header ======  Done ==
//                                       Padding(
//                                         padding: const EdgeInsets.symmetric(
//                                             horizontal: 0.5),
//                                         child: Container(
//                                           height: heightScreen / 14,
//                                           decoration: BoxDecoration(
//                                               color:
//                                                   modelOrder![indexOfAllOrder]
//                                                               .status !=
//                                                           'New'
//                                                       ? MyColors.orange
//                                                       : MyColors.lightGreen,
//                                               borderRadius:
//                                                   const BorderRadius.only(
//                                                       bottomLeft:
//                                                           Radius.circular(10),
//                                                       bottomRight:
//                                                           Radius.circular(10))),
//                                           child: Center(
//                                               child: MyText(
//                                                   title: modelOrder![
//                                                           indexOfAllOrder]
//                                                       .status)),
//                                         ),
//                                       ),
//                                       // === this info of order =======
//
//                                       Padding(
//                                         padding: const EdgeInsets.only(
//                                             left: 8,
//                                             right: 8,
//                                             top: 10,
//                                             bottom: 1),
//                                         child: infoOrder(
//                                             nameOwnOrder:
//                                                 modelOrder![indexOfAllOrder]
//                                                     .casher,
//                                             // numberOrder: orders[indexOfAllOrder].toString(),
//                                             tableNumber:
//                                                 modelOrder![indexOfAllOrder]
//                                                     .tableNumber
//                                                     .toString(),
//                                             timeExpected: '',
//                                             timeOrder:
//                                                 modelOrder![indexOfAllOrder]
//                                                     .time,
//                                             orderNumber:
//                                                 modelOrder![indexOfAllOrder]
//                                                     .orderNumber,
//                                             typeOrder:
//                                                 modelOrder![indexOfAllOrder]
//                                                     .orderStatus),
//                                       ),
//                                       Divider(
//                                         color: MyColors.gray,
//                                       ),
//                                       // ====== this list of orders =====
//                                       Expanded(
//                                         child: ListView.separated(
//                                           // it mean if items more than 5 don't show else 5
//                                           itemCount:
//                                               modelOrder![indexOfAllOrder]
//                                                           .items
//                                                           .length >
//                                                       5
//                                                   ? 5
//                                                   : modelOrder![indexOfAllOrder]
//                                                       .items
//                                                       .length,
//                                           padding: const EdgeInsets.symmetric(
//                                               horizontal: 2),
//                                           shrinkWrap: true,
//                                           itemBuilder: (context, indexOfItems) {
//                                             return order(
//                                                 isCustom:
//                                                     modelOrder![indexOfAllOrder]
//                                                         .items[indexOfItems]
//                                                         .isCustom,
//                                                 context: context,
//                                                 colorStateOrder:
//                                                     MyColors.lightGreen,
//                                                 des:
//                                                     modelOrder![indexOfAllOrder]
//                                                         .items[indexOfItems]
//                                                         .description,
//                                                 nameItem:
//                                                     modelOrder![indexOfAllOrder]
//                                                         .items[indexOfItems]
//                                                         .itemName,
//                                                 qun:
//                                                     modelOrder![indexOfAllOrder]
//                                                         .items[indexOfItems]
//                                                         .qty
//                                                         .toString());
//                                           },
//                                           //=====  Divider ====
//                                           separatorBuilder:
//                                               (context, indexOfItemsSeparator) {
//                                             return Padding(
//                                               padding:
//                                                   const EdgeInsets.symmetric(
//                                                       vertical: 0,
//                                                       horizontal: 4),
//                                               child: Divider(
//                                                 color: MyColors.gray,
//                                               ),
//                                             );
//                                           },
//                                         ),
//                                       ),
//                                       // ======= this button ==========
//                                       modelOrder![indexOfAllOrder]
//                                                   .items
//                                                   .length >
//                                               5
//                                           ? Padding(
//                                               padding:
//                                                   const EdgeInsets.symmetric(
//                                                       horizontal: 0.5),
//                                               child: Container(
//                                                 height: heightScreen / 14,
//                                                 decoration: BoxDecoration(
//                                                     color: MyColors.white,
//                                                     borderRadius:
//                                                         BorderRadius.circular(
//                                                             10)),
//                                                 child: Center(
//                                                     child: MyText(
//                                                         title: 'Continue ->',
//                                                         colorText:
//                                                             MyColors.black,
//                                                         size: 16)),
//                                               ),
//                                             )
//                                           : DoneButton(
//                                               onTap: () {
//                                                 Provider.of<HistoryProvider>(
//                                                         context,
//                                                         listen: false)
//                                                     .addToHistory(modelOrder![
//                                                         indexOfAllOrder]);
//                                                 Navigator.pop(context);
//                                               },
//                                               context: context,
//                                               color: MyColors.lightGreen,
//                                             )
//                                     ],
//                                   ),
//                                 ),
//                               ),
//                               modelOrder![indexOfAllOrder].items.length > 5
//                                   ? Padding(
//                                       padding: const EdgeInsets.all(8.0),
//                                       child: Container(
//                                         height: heightScreen,
//                                         width: widthScreen / 5,
//                                         decoration: BoxDecoration(
//                                             borderRadius:
//                                                 BorderRadius.circular(10),
//                                             color: MyColors.white),
//                                         child: Column(
//                                           children: [
//                                             // ===== this header ======
//                                             Padding(
//                                               padding:
//                                                   const EdgeInsets.symmetric(
//                                                       horizontal: 0.5),
//                                               child: Container(
//                                                 height: heightScreen / 14,
//                                                 decoration: BoxDecoration(
//                                                     color: MyColors.white,
//                                                     borderRadius:
//                                                         const BorderRadius.only(
//                                                             bottomLeft:
//                                                                 Radius.circular(
//                                                                     10),
//                                                             bottomRight:
//                                                                 Radius.circular(
//                                                                     10))),
//                                                 child: Center(
//                                                     child: MyText(
//                                                         title: '<- Continued')),
//                                               ),
//                                             ),
//                                             Divider(
//                                               color: MyColors.gray,
//                                             ),
//                                             // ====== this list of orders =====
//                                             Expanded(
//                                               child: ListView.separated(
//                                                 padding:
//                                                     const EdgeInsets.symmetric(
//                                                         horizontal: 2),
//                                                 shrinkWrap: true,
//                                                 itemCount:
//                                                     modelOrder![indexOfAllOrder]
//                                                             .items
//                                                             .length -
//                                                         5,
//                                                 itemBuilder: (context,
//                                                     indexOfItemOrders) {
//                                                   int indexOfItemOrdersTap2 =
//                                                       indexOfItemOrders + 5;
//                                                   return order(
//                                                       isCustom: modelOrder![
//                                                               indexOfAllOrder]
//                                                           .items[
//                                                               indexOfItemOrdersTap2]
//                                                           .isCustom,
//                                                       context: context,
//                                                       colorStateOrder:
//                                                           MyColors.lightGreen,
//                                                       des: modelOrder![
//                                                               indexOfAllOrder]
//                                                           .items[
//                                                               indexOfItemOrdersTap2]
//                                                           .description,
//                                                       nameItem: modelOrder![
//                                                               indexOfAllOrder]
//                                                           .items[
//                                                               indexOfItemOrdersTap2]
//                                                           .itemName,
//                                                       qun: modelOrder![
//                                                               indexOfAllOrder]
//                                                           .items[
//                                                               indexOfItemOrdersTap2]
//                                                           .qty
//                                                           .toString());
//                                                 },
//                                                 separatorBuilder: (context,
//                                                     indexOfItemOrdersSeparator) {
//                                                   return Padding(
//                                                     padding: const EdgeInsets
//                                                             .symmetric(
//                                                         vertical: 0,
//                                                         horizontal: 4),
//                                                     child: Divider(
//                                                       color: MyColors.gray,
//                                                     ),
//                                                   );
//                                                 },
//                                               ),
//                                             ),
//                                             DoneButton(
//                                               onTap: () {
//                                                 Provider.of<HistoryProvider>(
//                                                         context,
//                                                         listen: false)
//                                                     .addToHistory(modelOrder![
//                                                         indexOfAllOrder]);
//                                                 Navigator.pop(context);
//                                               },
//                                               context: context,
//                                               color: MyColors.lightGreen,
//                                             )
//                                           ],
//                                         ),
//                                       ),
//                                     )
//                                   : const SizedBox()
//                             ],
//                           );
//                         },
//                         shrinkWrap: true,
//                         separatorBuilder: (context, indexOfAllOrderSeparator) {
//                           return const SizedBox(
//                             width: 15,
//                           );
//                         },
//                       ),
//                     )
//                   : Center(
//                       child: CircularProgressIndicator(
//                         color: MyColors.green,
//                       ),
//                     )),
//     );
//   }
//
//   /// TODO : ======= Fake Data ===
// // ///
// // List<OrderModel> orders = [
// //   OrderModel(
// //       id: 0,
// //       time: '08:00',
// //       resTime: '08:30',
// //       OwnerName: 'Abdallah',
// //       type: 'Take a away',
// //       stateAllOrder: 'New',
// //       orders: [
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese Abdallah / order 1',
// //             state: 'complete',
// //             quantity: 4)
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '09:00',
// //       resTime: '09:30',
// //       OwnerName: 'Ali',
// //       type: 'Take a away /order2',
// //       stateAllOrder: 'New',
// //       orders: [
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese/ order2/ fisrt order',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese / last order',
// //             state: 'complete',
// //             quantity: 7),
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '08:00',
// //       resTime: '08:30',
// //       OwnerName: 'Abdallah',
// //       type: 'Take a away',
// //       stateAllOrder: 'Canceled',
// //       orders: [
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4)
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '09:00',
// //       resTime: '09:30',
// //       OwnerName: 'Ali',
// //       type: 'Take a away',
// //       stateAllOrder: 'New',
// //       orders: [
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '08:00',
// //       resTime: '08:30',
// //       OwnerName: 'Abdallah',
// //       type: 'Take a away',
// //       stateAllOrder: 'Canceled',
// //       orders: [
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4)
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '09:00',
// //       resTime: '09:30',
// //       OwnerName: 'Ali',
// //       type: 'Take a away',
// //       stateAllOrder: 'Canceled',
// //       orders: [
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '09:00',
// //       resTime: '09:30',
// //       OwnerName: 'Ali',
// //       type: 'Take a away',
// //       stateAllOrder: 'New',
// //       orders: [
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '08:00',
// //       resTime: '08:30',
// //       OwnerName: 'Abdallah',
// //       type: 'Take a away',
// //       stateAllOrder: 'Canceled',
// //       orders: [
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4)
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '09:00',
// //       resTime: '09:30',
// //       OwnerName: 'Ali',
// //       type: 'Take a away',
// //       stateAllOrder: 'New',
// //       orders: [
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '09:00',
// //       resTime: '09:30',
// //       OwnerName: 'Ali',
// //       type: 'Take a away',
// //       stateAllOrder: 'Canceled',
// //       orders: [
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '08:00',
// //       resTime: '08:30',
// //       OwnerName: 'Abdallah',
// //       type: 'Take a away',
// //       stateAllOrder: 'New',
// //       orders: [
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4)
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '09:00',
// //       resTime: '09:30',
// //       OwnerName: 'Ali',
// //       type: 'Take a away',
// //       stateAllOrder: 'New',
// //       orders: [
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '08:00',
// //       resTime: '08:30',
// //       OwnerName: 'Abdallah',
// //       type: 'Take a away',
// //       stateAllOrder: 'Canceled',
// //       orders: [
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4)
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '09:00',
// //       resTime: '09:30',
// //       OwnerName: 'Ali',
// //       type: 'Take a away',
// //       stateAllOrder: 'Canceled',
// //       orders: [
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //       ]),
// //   OrderModel(
// //       id: 0,
// //       time: '09:00',
// //       resTime: '09:30',
// //       OwnerName: 'Ali last order',
// //       type: 'TextRemove',
// //       stateAllOrder: 'Canceled',
// //       orders: [
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'Chicken',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 7),
// //         Order(
// //             name: 'Beef',
// //             des: 'Double / Extar / with cheese',
// //             state: 'complete',
// //             quantity: 4),
// //         Order(
// //             name: 'last item',
// //             des: 'Double / Extar / with cheese /last item',
// //             state: 'complete',
// //             quantity: 7),
// //       ]),
// // ];
// //
// //
// }

import 'dart:convert';
import 'dart:typed_data';
import 'package:app_app_kitchen/helper_fun/get_app_size.dart';
import 'package:app_app_kitchen/provider/history_data_provider.dart';
import 'package:app_app_kitchen/provider/home_data_provider.dart';
import 'package:app_app_kitchen/server_socket/create_server.dart';
import 'package:app_app_kitchen/testhelper/model_order/model_order.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/Constants/names.dart';
import 'package:app_app_kitchen/ui/helpers/alert_dialogs/alert_pop_scope_app.dart';
import 'package:app_app_kitchen/ui/helpers/app_bar/app_bar.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/buttons/done_button.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/info_order.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/order.dart';
import 'package:app_app_kitchen/ui/helpers/widgets/text.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:flutter_sound/public/flutter_sound_player.dart';
import 'package:new_keyboard_shortcuts/keyboard_shortcuts.dart';
import 'package:provider/provider.dart';

typedef Fn = void Function();

class HomePage extends StatefulWidget {
  final ipSocket;

  const HomePage({Key? key, required this.ipSocket}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  // ================================================================
  // ================================================================
  final FlutterSoundPlayer? _mPlayer = FlutterSoundPlayer();
  bool _mPlayerIsInited = false;
  AudioCache audioCache = AudioCache();

  void play(String audio) async {
    // ====================================
    final file = await audioCache.loadAsFile(audio);
    final bytes = await file.readAsBytes();
    // ====================================


    await _mPlayer!.startPlayer(
        fromDataBuffer: bytes,
        codec: Codec.mp3,
        whenFinished: () {
          setState(() {});
        });
    setState(() {});
  }

  Future<void> stopPlayer() async {
    if (_mPlayer != null) {
      await _mPlayer!.stopPlayer();
    }
  }

  // --------------------- UI -------------------

  getPlaybackFn(state) async {
    if (_mPlayerIsInited == false) {}
    if (_mPlayer!.isPlaying == true) {
      await stopPlayer();
      setState(() {

      });
    } else {
      if (state == "New") {
        play(ConstNames.New_Sound);
      }
      if (state == "Changed") {
        play(ConstNames.Changed_Sound);
      }
      if (state == "Done") {
        play(ConstNames.Done_Sound);
      }
      if (state == "Pop") {
        play(ConstNames.Pop_Sound);
      }
    }
  }


  // ================================================================
  // ================================================================
  final ScrollController _scrollController = ScrollController();
  late double position;
  Server? server;
  List ? modelOrderHomeDB;
  List<String> serverLogs = [];

  startServer() async {
    if (server!.running) {
      debugPrint("Server is already running");
    } else {
      await server!.start();
    }
  }


  Color colorItem = MyColors.lightGreen;

  Orders? dataFromSocket;

  onData5(Uint8List data) {
    // ================= convert data to String ================================
    String stringData = String.fromCharCodes(data);
    // ===================  Convert String to Map ==============================
    Map<String, dynamic> jsonFromString = json.decode(stringData.toString());
    // =================== Model Map in Order Model And Add To Data Base =======

    // =-=-  if Order Is new =-=-=-=-== Done ====
    if (jsonFromString['status'] == 'New') {
      setState(() {
        dataFromSocket = Orders.fromJson(jsonFromString);
      });
      for (var element in dataFromSocket!.items) {
        element.status_item = 'NewItem';
      }
      Provider.of<HomeProvider>(context, listen: false).addToHomeDB(dataFromSocket);
      setState(() {});

      getPlaybackFn("New");
    }


    if (jsonFromString['status'] == 'Changed') {
      setState(() {
        dataFromSocket = Orders.fromJson(jsonFromString);
      });

      // -=-=--=  Get index =-=-=-=-
      int indexOldOrderDB;
      int indexOrderDBInHistory;
      debugPrint("%%%%% the Order is Changed %%%%%%%");
      indexOldOrderDB =
          modelOrderHomeDB!.indexWhere((element) =>
          element['order_number'] ==
              dataFromSocket!.orderNumber);
      debugPrint(
          "===== the Index Old Order In DB ==== " + indexOldOrderDB.toString());
      indexOrderDBInHistory = Provider
          .of<HistoryProvider>(context, listen: false)
          .historyOrder
          .indexWhere((element) => element['order_number'] == dataFromSocket!.orderNumber);
      debugPrint(
          "=====----- the Index Old Order In History Page DB -----==== " +
              indexOrderDBInHistory.toString());
      // =-=-=-=- -=-=-=--  =-=-=-=- -==-=-
      // ====== First Case Is Order in home Page ======== Done ====
      if (indexOldOrderDB != -1 && indexOrderDBInHistory == -1) {
        for (var elementDB in modelOrderHomeDB![indexOldOrderDB]['items']) {
          for (var element in dataFromSocket!.items) {
            if (element.itemCode == elementDB['item_code']) {
              if (
              element.itemName != elementDB['item_name'] ||
                  element.description != elementDB['description'] ||
                  element.isCustom != elementDB['is_custom'] ||
                  element.isSup != elementDB['is_sup'] ||
                  element.qty != elementDB['qty']

              ) {
                setState(() {
                  element.status_item = 'ChangedItem';
                  // elementDB['status_item'] = 'ChangedItem' ;
                });
              }
              if (elementDB['status_item'] == 'DoneItem') {
                setState(() {
                  element.status_item = 'DoneItem';
                });
              }
            }
          }
        }
        Provider.of<HomeProvider>(context, listen: false).addToHomeDBInIndex(
            indexOldOrderDB, dataFromSocket);
        setState(() {});
      }

      // ====== sceonde Case Order in history Page =======

      if (indexOrderDBInHistory != -1 && indexOldOrderDB == -1) {
        for (var element in dataFromSocket!.items) {
          for (var elementDB in Provider
              .of<HistoryProvider>(context, listen: false)
              .historyOrder[indexOrderDBInHistory]['items']) {
            if (element.itemCode == elementDB['item_code']) {
              if (element.itemName == elementDB['item_name']) {
                setState(() {
                  element.status_item = 'DoneItem';
                });
              } else {
                setState(() {
                  element.status_item = 'NewItem';
                });
              }
            }
          }
        }

        setState(() {
          Provider.of<HistoryProvider>(context, listen: false).removeOneOrder(
              indexOrderDBInHistory);
          Provider.of<HomeProvider>(context, listen: false).addToHomeDB(dataFromSocket);
        });
      }

      setState(() {});


      setState(() {});
      getPlaybackFn("Changed");
    }
  }
  // =====================================================

  addNewOrder(){
    for (var element in dataFromSocket!.items) {
      element.status_item = 'NewItem';
      debugPrint("=== Item Code === "+element.itemCode.toString() +"===== status_item =====  "+ element.status_item.toString());
    }
    // Provider.of<HomeProvider>(context, listen: false).addToHomeDB(dataFromSocket);
    setState(() {
      Provider.of<HomeProvider>(context, listen: false).addToHomeDB(dataFromSocket);
    });
    setState(() {});
    // play notification Sound
    getPlaybackFn("New");
  }

  onData6(Uint8List data) {
    // ================= convert data to String ================================
    String stringData = String.fromCharCodes(data);
    // ===================  Convert String to Map ==============================
    Map<String, dynamic> jsonFromString = json.decode(stringData.toString());
    // =================== Model Map in Order Model And Add To Data Base =======

    setState(() {
      dataFromSocket = Orders.fromJson(jsonFromString);
    });

    // =-=-  if Order Is new =-=-=-=-== Done ====

    if (dataFromSocket!.status == 'New') {
     /* for (var element in dataFromSocket!.items) {
        element.status_item = 'NewItem';
        debugPrint("=== Item Code === "+element.itemCode.toString() +"===== status_item =====  "+ element.status_item.toString());
      }
      // Provider.of<HomeProvider>(context, listen: false).addToHomeDB(dataFromSocket);
      setState(() {
        Provider.of<HomeProvider>(context, listen: false).addToHomeDB(dataFromSocket);
      });
      setState(() {});
      // play notification Sound
      getPlaybackFn("New");*/

      addNewOrder();
    }
    // -=-=-=-=-= -=-=-=-=- =-=-=-=-=- =-=-=-=-=- =-=-=-=- =-=-=- -=-=-=-


    if (dataFromSocket!.status  == 'Changed') {
      debugPrint("%%%%% the Order is Changed %%%%%%%");
      // -=-=--=  Get index =-=-=-=-
      int indexOldOrderDBHomePage;
      int indexOrderDBInHistoryPage;

      indexOldOrderDBHomePage = modelOrderHomeDB!.indexWhere((element) => element['order_number'] ==  dataFromSocket!.orderNumber);
      debugPrint("=== the Index Old Order In DB ==== " + indexOldOrderDBHomePage.toString());
      indexOrderDBInHistoryPage = Provider.of<HistoryProvider>(context, listen: false).historyOrder.indexWhere((element) => element['order_number'] == dataFromSocket!.orderNumber);
      debugPrint("====--- the Index Old Order In History Page DB --==== " +indexOrderDBInHistoryPage.toString());
      // =-=-=-=- -=- First Case Is Order in home Page -=-=-=- -==-=-

      if (indexOldOrderDBHomePage != -1 && indexOrderDBInHistoryPage == -1) {





        for (var elementDB in modelOrderHomeDB![indexOldOrderDBHomePage]['items']) {
          for (var element in dataFromSocket!.items) {
            if (element.itemCode == elementDB['item_code']  ) {

              if(elementDB['status_item'] != 'DoneItem'){
                if (
                element.itemName != elementDB['item_name'] ||
                    element.description != elementDB['description'] ||
                    element.isCustom != elementDB['is_custom'] ||
                    element.isSup != elementDB['is_sup'] ||
                    element.qty != elementDB['qty']

                ){
                  element.status_item ='ChangedItem' ;
                }else{
                  element.status_item ='NewItem' ;
                }
              }



              if(elementDB['status_item'] == 'DoneItem'){

                if ( element.qty != elementDB['qty']

                ){
                  element.status_item ='ChangedItem' ;
                }else{
                  element.status_item ='DoneItem' ;
                }

              }


            /*
              if (
              element.itemName != elementDB['item_name'] ||
                  element.description != elementDB['description'] ||
                  element.isCustom != elementDB['is_custom'] ||
                  element.isSup != elementDB['is_sup'] ||
                  element.qty != elementDB['qty']

              ) {
                setState(() {
                  element.status_item = 'ChangedItem';
                  // elementDB['status_item'] = 'ChangedItem' ;
                });
              }
              if (elementDB['status_item'] == 'DoneItem') {
                setState(() {
                  element.status_item = 'DoneItem';
                });
              }
              */

            }
          }
        }
        setState(() {
          Provider.of<HomeProvider>(context, listen: false).addToHomeDBInIndex(indexOldOrderDBHomePage, dataFromSocket);
        });
        // Provider.of<HomeProvider>(context, listen: false).addToHomeDBInIndex(indexOldOrderDBHomePage, dataFromSocket);
        setState(() {});
      }

      // ====== sceonde Case Order in history Page =======

      if (indexOrderDBInHistoryPage != -1 && indexOldOrderDBHomePage == -1) {
        for (var element in dataFromSocket!.items) {
          for (var elementDB in Provider
              .of<HistoryProvider>(context, listen: false).historyOrder[indexOrderDBInHistoryPage]['items']) {



            if (element.itemCode == elementDB['item_code']) {
              if (element.itemName == elementDB['item_name'] && element.qty == elementDB['qty'] ) {

                  element.status_item = 'DoneItem';

              }
              if(element.itemName == elementDB['item_name'] && element.qty != elementDB['qty']){
                element.status_item = 'ChangedItem';
              }
              else {
                  element.status_item = 'NewItem';
              }
            }
          }
        }

        setState(() {
          Provider.of<HistoryProvider>(context, listen: false).removeOneOrder(indexOrderDBInHistoryPage);
          Provider.of<HomeProvider>(context, listen: false).addToHomeDB(dataFromSocket);
        });
      }

      setState(() {});

      getPlaybackFn("Changed");
    }

  }
  // =====================================================


  onError(dynamic error) {
    debugPrint(error.toString());
  }

  @override
  void initState() {
    super.initState();
    Provider.of<HomeProvider>(context, listen: false).initialDataHome();
    modelOrderHomeDB = Provider
        .of<HomeProvider>(context, listen: false)
        .homeOrders;
    setState(() {

    });

    // ======================= Start Server ======================
    server = Server(
      ipSocket: widget.ipSocket,
      onData: onData6,
      onError: onError,
      context: context,
    );
    startServer();
    Provider.of<HistoryProvider>(context, listen: false).initalData();
    position = _scrollController.initialScrollOffset;
    _scrollController.addListener(() {
      position = _scrollController.offset;
    });

    // ======================

    _mPlayer!.openAudioSession().then((value) {
      setState(() {
        _mPlayerIsInited = true;
      });
    });
  }


  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    modelOrderHomeDB = Provider
        .of<HomeProvider>(context, listen: false)
        .homeOrders;
    double widthScreen = getScreenWidth(context);
    double heightScreen = getScreenHeight(context);
    return PopScopeApp(
      child: Scaffold(
          appBar: myAppBar(
            ipAddress: widget.ipSocket.toString(),
            context: context,
            title: ConstNames.Home_Page,
            onTapNext: () {
              print("pressed");
              if (position != _scrollController.position.maxScrollExtent) {
                _scrollController.jumpTo(position + (widthScreen - 10));
              }
              // KeyBoardShortcuts(
              //
              //   onKeysPressed: (){
              //     print("on Preesed");
              //     if (position != _scrollController.position.maxScrollExtent) {
              //       _scrollController.jumpTo(position + (widthScreen - 10));
              //     }
              //
              //   },
              //   keysToPress:{LogicalKeyboardKey.shift,LogicalKeyboardKey.keyD},
              // );

            },
            onTapBack: () {
              _scrollController.animateTo(position - 170,
                  duration: const Duration(milliseconds: 250),
                  curve: Curves.bounceInOut);
            },
          ),
          body:
          modelOrderHomeDB != null || modelOrderHomeDB!.isNotEmpty
              ? Center(
            child: ListView.builder(
              controller: _scrollController,
              scrollDirection: Axis.horizontal,
              itemCount: modelOrderHomeDB!.length,
              itemBuilder: (context, indexOfAllOrder) {
                return Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(6.0),
                      child: Container(
                        height: heightScreen,
                        width: widthScreen / 5,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: MyColors.white),
                        child: Column(
                          children: [
                            // ===== this header ======  Done ==
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 0.5),
                              child: Container(
                                height: heightScreen / 14,
                                decoration: BoxDecoration(
                                    color: modelOrderHomeDB![indexOfAllOrder]['status'] !=
                                        'New'
                                        ? MyColors.orange
                                        : MyColors.lightGreen,
                                    borderRadius:
                                    const BorderRadius.only(
                                        bottomLeft:
                                        Radius.circular(1),
                                        bottomRight:
                                        Radius.circular(1))),
                                child: Center(
                                    child: MyText(
                                        title: modelOrderHomeDB![indexOfAllOrder]['status'])
                                ),
                              ),
                            ),
                            // === this info of order =======

                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 8,
                                  right: 8,
                                  top: 10,
                                  bottom: 1),
                              child: infoOrder(
                                  nameOwnOrder:
                                  modelOrderHomeDB![indexOfAllOrder]
                                  ['casher'],
                                  // numberOrder: orders[indexOfAllOrder].toString(),
                                  tableNumber:
                                  modelOrderHomeDB![indexOfAllOrder]['table_number']
                                      .toString(),
                                  timeExpected: '',
                                  timeOrder:
                                  modelOrderHomeDB![indexOfAllOrder]
                                  ['time'],
                                  orderNumber:
                                  modelOrderHomeDB![indexOfAllOrder]
                                  ['order_number'],
                                  typeOrder:
                                  modelOrderHomeDB![indexOfAllOrder]
                                  ['order_status']),
                            ),
                            Divider(
                              color: MyColors.gray,
                            ),
                            // ====== this list of orders =====
                            Expanded(
                              child: ListView.separated(
                                // it mean if items more than 5 don't show else 5
                                itemCount: modelOrderHomeDB![indexOfAllOrder]['items']
                                    .length >
                                    5
                                    ? 5
                                    : modelOrderHomeDB![indexOfAllOrder]['items']
                                    .length,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 2),
                                shrinkWrap: true,
                                itemBuilder: (context, indexOfItems) {
                                  return order(
                                      isCustom: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItems]['is_custom'],
                                      context: context,
                                      // colorStateOrder: colorItems(
                                      //     indexOfAllOrder, indexOfItems),
                                      colorStateOrder: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItems]['status_item'] ==
                                          'ChangedItem' ? MyColors.orange
                                          : modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItems]['status_item'] ==
                                          'DoneItem' ? MyColors.blue
                                          : MyColors.lightGreen,
                                      des: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItems]['description'],
                                      nameItem: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItems]['item_name'],
                                      overLine: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItems]['status_item'] ==
                                          'DoneItem' ? true : false,

                                      qun: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItems]['qty']
                                          .toString());
                                },
                                //=====  Divider ====
                                separatorBuilder:
                                    (context, indexOfItemsSeparator) {
                                  return Divider(
                                    color: MyColors.gray,
                                  );
                                },
                              ),
                            ),

                            // ======= this button ==========
                            modelOrderHomeDB![indexOfAllOrder]['items'].length >
                                5
                                ? Padding(
                              padding:
                              const EdgeInsets.symmetric(
                                  horizontal: 15),
                              child: Container(
                                height: heightScreen / 14,
                                decoration: BoxDecoration(
                                    color: MyColors.white,
                                    borderRadius:
                                    BorderRadius.circular(
                                        0)),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.end,
                                  children: [
                                    MyText(
                                        title: ' Continue ${ modelOrderHomeDB![indexOfAllOrder]
                                        ['order_number']}',
                                        colorText:
                                        MyColors.black,
                                        size: 16),
                                    Icon(Icons.arrow_forward,
                                        size: 20,
                                        color: MyColors.gray)
                                  ],
                                ),
                              ),
                            )
                                : DoneButton(
                                onTap: () {
                                  // ====== Add to data Base ========
                                  Provider.of<HistoryProvider>(
                                      context,
                                      listen: false)
                                      .addToHistoryDB(modelOrderHomeDB![
                                  indexOfAllOrder]);
                                  //  // ======== Remove from Home =========
                                  //
                                  Provider.of<HomeProvider>(
                                      context, listen: false)
                                      .removeOneOrderFromHome(
                                      indexOfAllOrder);

                                  setState(() {});
                                  // ==== Close the Alert =======

                                  Navigator.pop(context);
                                  getPlaybackFn("Done");
                                },
                                context: context,
                                color: MyColors.green,
                                tableNumber:
                                modelOrderHomeDB![indexOfAllOrder]
                                ['table_number'],
                                orderNumber:
                                modelOrderHomeDB![indexOfAllOrder]
                                ['order_number'],
                                nameOwnOrder:
                                modelOrderHomeDB![indexOfAllOrder]
                                ['casher'],
                                timeOrder:
                                modelOrderHomeDB![indexOfAllOrder]
                                ['time'],
                                typeOrder:
                                modelOrderHomeDB![indexOfAllOrder]
                                ['order_status'])
                          ],
                        ),
                      ),
                    ),
                    modelOrderHomeDB![indexOfAllOrder]['items'].length > 5
                        ? SizedBox(
                      width: (widthScreen / 4.65) *
                          ((modelOrderHomeDB![indexOfAllOrder]['items'].length -
                              5) / 5).ceil(),
                      child: ListView.separated(
                        separatorBuilder: (context, index) => Divider(color: MyColors.gray),
                        physics:
                        const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: ((modelOrderHomeDB![indexOfAllOrder]['items']
                            .length - 5) / 5).ceil(),
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(9),
                            child: Container(
                              height: heightScreen,
                              width: widthScreen / 5,
                              decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.circular(10),
                                  color: MyColors.white),
                              child: Column(
                                children: [
                                  // ===== this header ======
                                  Padding(
                                    padding: const EdgeInsets
                                        .symmetric(
                                        horizontal: 0.5),
                                    child: Container(
                                      height: heightScreen / 14,
                                      decoration: BoxDecoration(
                                          color: MyColors.white,
                                          borderRadius:
                                          const BorderRadius
                                              .only(
                                              bottomLeft: Radius
                                                  .circular(
                                                  10),
                                              bottomRight: Radius
                                                  .circular(
                                                  10))),
                                      child: Padding(
                                        padding:
                                        const EdgeInsets
                                            .symmetric(
                                            horizontal: 15),
                                        child: Container(
                                          height:
                                          heightScreen / 14,
                                          decoration: BoxDecoration(
                                              color: MyColors
                                                  .white,
                                              borderRadius:
                                              BorderRadius
                                                  .circular(
                                                  5)),
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment
                                                .start,
                                            children: [
                                              Icon(
                                                  Icons
                                                      .arrow_back,
                                                  size: 20,
                                                  color: MyColors
                                                      .gray),
                                              MyText(
                                                  title:
                                                  ' Continue ',
                                                  colorText:
                                                  MyColors
                                                      .black,
                                                  size: 16),

                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Divider(
                                    color: MyColors.gray,
                                  ),
                                  // ====== this list of orders =====
                                  Expanded(
                                    child: ListView.separated(
                                      padding: const EdgeInsets
                                          .symmetric(
                                          horizontal: 2),
                                      shrinkWrap: true,
                                      itemCount:

                                      modelOrderHomeDB![ indexOfAllOrder]['items']
                                          .length > (10 + (index * 5))
                                          ? 5
                                          : (modelOrderHomeDB![indexOfAllOrder]['items']
                                          .length - (5 + (index * 5))),
                                      itemBuilder: (context,
                                          indexOfItemOrders) {
                                        int indexOfItemOrdersTap2 =
                                            (index * 5) +
                                                5 +
                                                indexOfItemOrders;


                                        return order(
                                            isCustom: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItemOrdersTap2]['is_custom'],
                                            context: context,

                                            colorStateOrder: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItemOrdersTap2]['status_item'] ==
                                                'ChangedItem' ? MyColors.orange
                                                : modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItemOrdersTap2]['status_item'] ==
                                                'DoneItem' ? MyColors.blue
                                                : MyColors.lightGreen,
                                            des: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItemOrdersTap2]['description'],
                                            nameItem: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItemOrdersTap2]['item_name'],
                                            overLine: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItemOrdersTap2]['status_item'] ==
                                                'DoneItem' ? true : false,
                                            qun: modelOrderHomeDB![indexOfAllOrder]['items'][indexOfItemOrdersTap2]['qty']
                                                .toString());
                                      },
                                      separatorBuilder: (context,
                                          indexOfItemOrdersSeparator) {
                                        return Padding(
                                          padding:
                                          const EdgeInsets
                                              .symmetric(
                                              vertical: 0,
                                              horizontal:
                                              4),
                                          child: Divider(
                                            color:
                                            MyColors.gray,
                                          ),
                                        );
                                      },
                                    ),
                                  ),

                                  modelOrderHomeDB![indexOfAllOrder]['items']
                                      .length >
                                      (10 + (index * 5))
                                      ? Padding(
                                    padding:
                                    const EdgeInsets
                                        .symmetric(
                                        horizontal:
                                        15),
                                    child: Container(
                                      height:
                                      heightScreen /
                                          14,
                                      decoration: BoxDecoration(
                                          color: MyColors
                                              .white,
                                          borderRadius:
                                          BorderRadius
                                              .circular(
                                              0)),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .end,
                                        children: [
                                          MyText(
                                              title:
                                              ' Continue ${modelOrderHomeDB![ indexOfAllOrder]['order_number']}',
                                              colorText:
                                              MyColors
                                                  .black,
                                              size: 16),
                                          Icon(
                                            Icons
                                                .arrow_forward,
                                            size: 20,
                                            color:
                                            MyColors
                                                .gray,
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                      : DoneButton(
                                      onTap: () {
                                        //  ====== Add to data Base ========
                                        Provider.of<HistoryProvider>(
                                            context,
                                            listen:
                                            false)
                                            .addToHistoryDB(
                                            modelOrderHomeDB![indexOfAllOrder]);
                                        // // ======== Remove from Home =========
                                        Provider.of<HomeProvider>(
                                            context, listen: false)
                                            .removeOneOrderFromHome(
                                            indexOfAllOrder);
                                        setState(() {});
                                        // ==== Close the Alert =======

                                        Navigator.pop(
                                            context);
                                        getPlaybackFn("Done");
                                      },
                                      context: context,
                                      color: MyColors.green,
                                      tableNumber: modelOrderHomeDB![ indexOfAllOrder]['table_number'],
                                      orderNumber: modelOrderHomeDB![ indexOfAllOrder]['order_number'],
                                      nameOwnOrder: modelOrderHomeDB![ indexOfAllOrder]['casher'],
                                      timeOrder: modelOrderHomeDB![indexOfAllOrder]['time'],
                                      typeOrder: modelOrderHomeDB![ indexOfAllOrder]['order_status']
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    )
                        : const SizedBox()
                  ],
                );
              },
              shrinkWrap: true,
            ),
          )
              : const Center(
            child: Text("No orders Yet"),
          )),
    );
  }


}


import 'package:app_app_kitchen/provider/home_data_provider.dart';
import 'package:app_app_kitchen/server_socket/get_ip_network/provider_get_ip.dart';
import 'package:app_app_kitchen/ui/Constants/colors.dart';
import 'package:app_app_kitchen/ui/helpers/alert_dialogs/alert_pop_scope_app.dart';
import 'package:app_app_kitchen/ui/screens/home_screeen/home_page.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class StartApp extends StatefulWidget {
  const StartApp({Key? key}) : super(key: key);

  @override
  _StartAppState createState() => _StartAppState();
}

class _StartAppState extends State<StartApp> {
  String? ipSocket;

  @override
  Widget build(BuildContext context) {
    return PopScopeApp(
      child: Scaffold(
        body: Center(
          child: Container(
            decoration: BoxDecoration(color: MyColors.green),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextButton(
                child: Text(
                  " Start Create Server ",
                  style: TextStyle(color: MyColors.black, fontSize: 25),
                ),
                onPressed: () {
                  Provider.of<GetIpNetworkProvider>(context, listen: false)
                      .initNetworkInfo()
                      .then((value) {
                    print(Provider.of<GetIpNetworkProvider>(context,
                            listen: false)
                        .ipSocket);
                    ipSocket = Provider.of<GetIpNetworkProvider>(context,
                            listen: false)
                        .ipSocket;
                    print("Socket " + ipSocket!);
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => HomePage(
                            ipSocket: ipSocket,
                          ),
                        ));
                  });
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    Provider.of<GetIpNetworkProvider>(context, listen: false).initNetworkInfo();
    Provider.of<HomeProvider>(context, listen: false).initialDataHome();
  }
}
